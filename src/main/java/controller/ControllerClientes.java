package controller;

import entity.TbClientesEntity;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class ControllerClientes

{

    @FXML
    private ListView<TbClientesEntity> lvlistaclientes;
    Stage primaryStage = new Stage();
    private Thread th;

    public void onclickalta(javafx.event.ActionEvent actionEvent) throws Exception {

        URL url = new File("src/main/resources/view/clienteAlta.fxml").toURL();
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("MenuClientes");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

    }

    public void initialize() {


        // Personalitzem la CellFactory
        lvlistaclientes.setCellFactory((list) -> {
            return new myListCell();
        });

        // El mateix es podria fer amb una classe anònima en lloc de crear myListCell
        /*lvLlistaCartes.setCellFactory((list) -> {
            return new ListCell<Person>() {
                @Override
                public void updateItem(Person item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setGraphic(null);
                    } else {
                        setGraphic(new ImageView(item.getImg()));
                        setText(item.getName());
                    }
                }
            };
        })*/
        ;

        // Afegir llista observable d'items
        ObservableList<TbClientesEntity> persons = FXCollections.observableArrayList(
                new TbClientesEntity("Julia", ""), new TbClientesEntity("Greta", ""));
        lvlistaclientes.setItems(persons);

        // Afegir un item
        lvlistaclientes.getItems().add(new TbClientesEntity("Pepi", ""));

        // Handle ListView selection changes with a listener
        lvlistaclientes.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                }
        );

        th = null;
    }

    public class myListCell extends ListCell<TbClientesEntity> {
        @Override
        public void updateItem(TbClientesEntity item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setGraphic(null);
            } else {
                //setGraphic((item.getNombre()));
                //setText(item.getSegundoApellido());
                setText("Prova");
            }
        }
    }

}
