
package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.query.Query;
import javax.persistence.metamodel.EntityType;
import java.io.File;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Obtengola ubicación del fxml principal mediante new File porque mediante el getClass.getResource no funcionaba
        URL url = new File("src/main/resources/view/Menuinicial.fxml").toURL();
//        Parent root = FXMLLoader.load(getClass().getResource("/src/main/resources/view/clienteAlta.fxml"));
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("Balart Nuvies");
        primaryStage.setScene(new Scene(root, 550, 615));
        primaryStage.show();
    }


    public static void main(String[] args) {
        final Session session = HibernateUtil.getSession();
        launch(args);

        //El siguiente código es para probar que hay conexión con la Base de Datos y que funcionan las Entidades creadas
        //Se puede comentar el siguiente código si no se necesitar hacer ninguna prueba
        try {
            System.out.println("querying all the managed entities...");
            final Metamodel metamodel = session.getSessionFactory().getMetamodel();
            for (EntityType<?> entityType : metamodel.getEntities()) {
                final String entityName = entityType.getName();
                final Query query = session.createQuery("from " + entityName);
                System.out.println("executing: " + query.getQueryString());
                for (Object o : query.list()) {
                    System.out.println("  " + o);
                }
            }
        } finally {
            session.close();
        }
    }


//    @Override
//    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("/src/main/resources/view/Menuinicial.fxml"));
//        primaryStage.setTitle("Menu");
//        primaryStage.setScene(new Scene(root, 600, 400));
//        primaryStage.show();
//    }
}