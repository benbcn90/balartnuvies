package controller;

import entity.TbClientesEntity;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;


public class ControllerMenu {
    private ListView<TbClientesEntity> lvLlistaCartes;
    Stage primaryStage = new Stage();
    public void onclickclientes(javafx.event.ActionEvent actionEvent) throws Exception{
        URL url = new File("src/main/resources/view/MenuClientes.fxml").toURL();
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("MenuClientes");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();


    }



    public void onclickproductos(ActionEvent actionEvent) throws Exception{
        URL url = new File("src/main/resources/view/MenuProductos.fxml").toURL();
        Parent root = FXMLLoader.load(url);

        primaryStage.setTitle("MenuProductos");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();

    }

    public void onclickventas(ActionEvent actionEvent) throws Exception{
        URL url = new File("src/main/resources/view/MenuVentas.fxml").toURL();
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("MenuVentas");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }
}
