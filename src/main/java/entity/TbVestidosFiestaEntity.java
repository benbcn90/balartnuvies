package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_vestidos_fiesta", schema = "balartnuvies")
public class
TbVestidosFiestaEntity {
    private int idVestidosFiesta;
    private Byte pedreria;
    private String actualwrk;
    private int tbTiposVestidosIdTipoVestido;
    private int idTipoEscote;
    private int referenciaProducto;

    @Id
    @Column(name = "idVestidosFiesta")
    public int getIdVestidosFiesta() {
        return idVestidosFiesta;
    }

    public void setIdVestidosFiesta(int idVestidosFiesta) {
        this.idVestidosFiesta = idVestidosFiesta;
    }

    @Basic
    @Column(name = "pedreria")
    public Byte getPedreria() {
        return pedreria;
    }

    public void setPedreria(Byte pedreria) {
        this.pedreria = pedreria;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "tb_tipos_vestidos_idTipoVestido")
    public int getTbTiposVestidosIdTipoVestido() {
        return tbTiposVestidosIdTipoVestido;
    }

    public void setTbTiposVestidosIdTipoVestido(int tbTiposVestidosIdTipoVestido) {
        this.tbTiposVestidosIdTipoVestido = tbTiposVestidosIdTipoVestido;
    }

    @Basic
    @Column(name = "idTipoEscote")
    public int getIdTipoEscote() {
        return idTipoEscote;
    }

    public void setIdTipoEscote(int idTipoEscote) {
        this.idTipoEscote = idTipoEscote;
    }

    @Basic
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbVestidosFiestaEntity that = (TbVestidosFiestaEntity) o;
        return tbTiposVestidosIdTipoVestido == that.tbTiposVestidosIdTipoVestido &&
                idTipoEscote == that.idTipoEscote &&
                referenciaProducto == that.referenciaProducto &&
                Objects.equals(pedreria, that.pedreria) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pedreria, actualwrk, tbTiposVestidosIdTipoVestido, idTipoEscote, referenciaProducto);
    }

}
