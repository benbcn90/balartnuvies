package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_ventas", schema = "balartnuvies")
public class TbVentasEntity {
    private int idVenta;
    private String fechaVenta;
    private String fechaEntrega;
    private Double importe;
    private Double descuento;
    private Double importePagado;
    private String actualwrk;
    private int idCliente;
    private int idPromocion;

    @Id
    @Column(name = "idVenta")
    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    @Basic
    @Column(name = "fechaVenta")
    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    @Basic
    @Column(name = "fechaEntrega")
    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @Basic
    @Column(name = "importe")
    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }

    @Basic
    @Column(name = "descuento")
    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    @Basic
    @Column(name = "importePagado")
    public Double getImportePagado() {
        return importePagado;
    }

    public void setImportePagado(Double importePagado) {
        this.importePagado = importePagado;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "idCliente")
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "idPromocion")
    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbVentasEntity that = (TbVentasEntity) o;
        return idVenta == that.idVenta &&
                idCliente == that.idCliente &&
                idPromocion == that.idPromocion &&
                Objects.equals(fechaVenta, that.fechaVenta) &&
                Objects.equals(fechaEntrega, that.fechaEntrega) &&
                Objects.equals(importe, that.importe) &&
                Objects.equals(descuento, that.descuento) &&
                Objects.equals(importePagado, that.importePagado) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVenta, fechaVenta, fechaEntrega, importe, descuento, importePagado, actualwrk, idCliente, idPromocion);
    }
}
