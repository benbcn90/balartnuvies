package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_clientes", schema = "balartnuvies")
public class TbClientesEntity {
    private int idCliente;
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String nif;
    private String direccion;
    private String codigoPostal;
    private String municipio;
    private String provincia;
    private String mail;
    private String sexo;
    private String fechaNacimiento;
    private String fechaEvento;
    private String fechaAlta;
    private String estadoCivil;
    private String telefonoPrincipal;
    private String telefonoSecundario;
    private String idioma;
    private String comentarios;
    private String estatura;
    private String contornoPecho;
    private String contornoCintura;
    private String contornoCadera;
    private String largoEspalda;
    private String largoBrazo;
    private String comoNosConocen;
    private String actualwrk;
    private int tbPaisesIdPais;

    public TbClientesEntity(String julia, String s) {
    }

    @Id
    @Column(name = "idCliente")
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "primerApellido")
    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Basic
    @Column(name = "segundoApellido")
    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Basic
    @Column(name = "nif")
    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "codigoPostal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Basic
    @Column(name = "municipio")
    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    @Basic
    @Column(name = "provincia")
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Basic
    @Column(name = "mail")
    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "fechaNacimiento")
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "fechaEvento")
    public String getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(String fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    @Basic
    @Column(name = "fechaAlta")
    public String getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(String fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    @Basic
    @Column(name = "estadoCivil")
    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Basic
    @Column(name = "telefonoPrincipal")
    public String getTelefonoPrincipal() {
        return telefonoPrincipal;
    }

    public void setTelefonoPrincipal(String telefonoPrincipal) {
        this.telefonoPrincipal = telefonoPrincipal;
    }

    @Basic
    @Column(name = "telefonoSecundario")
    public String getTelefonoSecundario() {
        return telefonoSecundario;
    }

    public void setTelefonoSecundario(String telefonoSecundario) {
        this.telefonoSecundario = telefonoSecundario;
    }

    @Basic
    @Column(name = "idioma")
    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    @Basic
    @Column(name = "comentarios")
    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    @Basic
    @Column(name = "estatura")
    public String getEstatura() {
        return estatura;
    }

    public void setEstatura(String estatura) {
        this.estatura = estatura;
    }

    @Basic
    @Column(name = "contornoPecho")
    public String getContornoPecho() {
        return contornoPecho;
    }

    public void setContornoPecho(String contornoPecho) {
        this.contornoPecho = contornoPecho;
    }

    @Basic
    @Column(name = "contornoCintura")
    public String getContornoCintura() {
        return contornoCintura;
    }

    public void setContornoCintura(String contornoCintura) {
        this.contornoCintura = contornoCintura;
    }

    @Basic
    @Column(name = "contornoCadera")
    public String getContornoCadera() {
        return contornoCadera;
    }

    public void setContornoCadera(String contornoCadera) {
        this.contornoCadera = contornoCadera;
    }

    @Basic
    @Column(name = "largoEspalda")
    public String getLargoEspalda() {
        return largoEspalda;
    }

    public void setLargoEspalda(String largoEspalda) {
        this.largoEspalda = largoEspalda;
    }

    @Basic
    @Column(name = "largoBrazo")
    public String getLargoBrazo() {
        return largoBrazo;
    }

    public void setLargoBrazo(String largoBrazo) {
        this.largoBrazo = largoBrazo;
    }

    @Basic
    @Column(name = "comoNosConocen")
    public String getComoNosConocen() {
        return comoNosConocen;
    }

    public void setComoNosConocen(String comoNosConocen) {
        this.comoNosConocen = comoNosConocen;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "tb_paises_idPais")
    public int getTbPaisesIdPais() {
        return tbPaisesIdPais;
    }

    public void setTbPaisesIdPais(int tbPaisesIdPais) {
        this.tbPaisesIdPais = tbPaisesIdPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbClientesEntity that = (TbClientesEntity) o;
        return idCliente == that.idCliente &&
                tbPaisesIdPais == that.tbPaisesIdPais &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(primerApellido, that.primerApellido) &&
                Objects.equals(segundoApellido, that.segundoApellido) &&
                Objects.equals(nif, that.nif) &&
                Objects.equals(direccion, that.direccion) &&
                Objects.equals(codigoPostal, that.codigoPostal) &&
                Objects.equals(municipio, that.municipio) &&
                Objects.equals(provincia, that.provincia) &&
                Objects.equals(mail, that.mail) &&
                Objects.equals(sexo, that.sexo) &&
                Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
                Objects.equals(fechaEvento, that.fechaEvento) &&
                Objects.equals(fechaAlta, that.fechaAlta) &&
                Objects.equals(estadoCivil, that.estadoCivil) &&
                Objects.equals(telefonoPrincipal, that.telefonoPrincipal) &&
                Objects.equals(telefonoSecundario, that.telefonoSecundario) &&
                Objects.equals(idioma, that.idioma) &&
                Objects.equals(comentarios, that.comentarios) &&
                Objects.equals(estatura, that.estatura) &&
                Objects.equals(contornoPecho, that.contornoPecho) &&
                Objects.equals(contornoCintura, that.contornoCintura) &&
                Objects.equals(contornoCadera, that.contornoCadera) &&
                Objects.equals(largoEspalda, that.largoEspalda) &&
                Objects.equals(largoBrazo, that.largoBrazo) &&
                Objects.equals(comoNosConocen, that.comoNosConocen) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCliente, nombre, primerApellido, segundoApellido, nif, direccion, codigoPostal, municipio, provincia, mail, sexo, fechaNacimiento, fechaEvento, fechaAlta, estadoCivil, telefonoPrincipal, telefonoSecundario, idioma, comentarios, estatura, contornoPecho, contornoCintura, contornoCadera, largoEspalda, largoBrazo, comoNosConocen, actualwrk, tbPaisesIdPais);
    }
}
