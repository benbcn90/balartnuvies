package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_productos", schema = "balartnuvies")
public class TbProductosEntity {
    private int referenciaProducto;
    private String nombre;
    private String descripcion;
    private Double precio;
    private Double iva;
    private Double recargo;
    private Double margen;
    private Double pvp;
    private Byte alertaStock;
    private Integer minAlertaStock;
    private Byte descatalogado;
    private String observaciones;
    private String actualwrk;
    private int tbColoresIdColor;
    private int idColeccion;
    private int idMarca;

    @Id
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "precio")
    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "iva")
    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    @Basic
    @Column(name = "recargo")
    public Double getRecargo() {
        return recargo;
    }

    public void setRecargo(Double recargo) {
        this.recargo = recargo;
    }

    @Basic
    @Column(name = "margen")
    public Double getMargen() {
        return margen;
    }

    public void setMargen(Double margen) {
        this.margen = margen;
    }

    @Basic
    @Column(name = "pvp")
    public Double getPvp() {
        return pvp;
    }

    public void setPvp(Double pvp) {
        this.pvp = pvp;
    }

    @Basic
    @Column(name = "alertaStock")
    public Byte getAlertaStock() {
        return alertaStock;
    }

    public void setAlertaStock(Byte alertaStock) {
        this.alertaStock = alertaStock;
    }

    @Basic
    @Column(name = "minAlertaStock")
    public Integer getMinAlertaStock() {
        return minAlertaStock;
    }

    public void setMinAlertaStock(Integer minAlertaStock) {
        this.minAlertaStock = minAlertaStock;
    }

    @Basic
    @Column(name = "descatalogado")
    public Byte getDescatalogado() {
        return descatalogado;
    }

    public void setDescatalogado(Byte descatalogado) {
        this.descatalogado = descatalogado;
    }

    @Basic
    @Column(name = "observaciones")
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "tb_colores_idColor")
    public int getTbColoresIdColor() {
        return tbColoresIdColor;
    }

    public void setTbColoresIdColor(int tbColoresIdColor) {
        this.tbColoresIdColor = tbColoresIdColor;
    }

    @Basic
    @Column(name = "idColeccion")
    public int getIdColeccion() {
        return idColeccion;
    }

    public void setIdColeccion(int idColeccion) {
        this.idColeccion = idColeccion;
    }

    @Basic
    @Column(name = "idMarca")
    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbProductosEntity that = (TbProductosEntity) o;
        return referenciaProducto == that.referenciaProducto &&
                tbColoresIdColor == that.tbColoresIdColor &&
                idColeccion == that.idColeccion &&
                idMarca == that.idMarca &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(descripcion, that.descripcion) &&
                Objects.equals(precio, that.precio) &&
                Objects.equals(iva, that.iva) &&
                Objects.equals(recargo, that.recargo) &&
                Objects.equals(margen, that.margen) &&
                Objects.equals(pvp, that.pvp) &&
                Objects.equals(alertaStock, that.alertaStock) &&
                Objects.equals(minAlertaStock, that.minAlertaStock) &&
                Objects.equals(descatalogado, that.descatalogado) &&
                Objects.equals(observaciones, that.observaciones) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(referenciaProducto, nombre, descripcion, precio, iva, recargo, margen, pvp, alertaStock, minAlertaStock, descatalogado, observaciones, actualwrk, tbColoresIdColor, idColeccion, idMarca);
    }
}
