package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_proveedores", schema = "balartnuvies")
public class TbProveedoresEntity {
    private int idProveedor;
    private String nombre;
    private String codigo;
    private String direccion;
    private String telefono;
    private String email;
    private String personaContacto;
    private String vigente;
    private String actualwrk;

    @Id
    @Column(name = "idProveedor")
    public int getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "personaContacto")
    public String getPersonaContacto() {
        return personaContacto;
    }

    public void setPersonaContacto(String personaContacto) {
        this.personaContacto = personaContacto;
    }

    @Basic
    @Column(name = "vigente")
    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbProveedoresEntity that = (TbProveedoresEntity) o;
        return idProveedor == that.idProveedor &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(codigo, that.codigo) &&
                Objects.equals(direccion, that.direccion) &&
                Objects.equals(telefono, that.telefono) &&
                Objects.equals(email, that.email) &&
                Objects.equals(personaContacto, that.personaContacto) &&
                Objects.equals(vigente, that.vigente) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProveedor, nombre, codigo, direccion, telefono, email, personaContacto, vigente, actualwrk);
    }
}
