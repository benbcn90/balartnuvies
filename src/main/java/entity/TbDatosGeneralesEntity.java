package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_datos_generales", schema = "balartnuvies")
public class TbDatosGeneralesEntity {
    private int idDatosGenerales;
    private int nombre;
    private String valor;
    private String actualwrk;

    @Id
    @Column(name = "idDatosGenerales")
    public int getIdDatosGenerales() {
        return idDatosGenerales;
    }

    public void setIdDatosGenerales(int idDatosGenerales) {
        this.idDatosGenerales = idDatosGenerales;
    }

    @Basic
    @Column(name = "nombre")
    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "valor")
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbDatosGeneralesEntity that = (TbDatosGeneralesEntity) o;
        return nombre == that.nombre &&
                Objects.equals(valor, that.valor) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, valor, actualwrk);
    }

}
