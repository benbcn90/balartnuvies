package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_promociones", schema = "balartnuvies")
public class TbPromocionesEntity {
    private int idPromocion;
    private String nombre;
    private String descripcion;
    private String vigente;
    private String actualwrk;

    @Id
    @Column(name = "idPromocion")
    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "vigente")
    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbPromocionesEntity that = (TbPromocionesEntity) o;
        return idPromocion == that.idPromocion &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(descripcion, that.descripcion) &&
                Objects.equals(vigente, that.vigente) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPromocion, nombre, descripcion, vigente, actualwrk);
    }
}
