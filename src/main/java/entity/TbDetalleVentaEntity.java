package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_detalle_venta", schema = "balartnuvies")
public class TbDetalleVentaEntity {
    private int idDetalleVenta;
    private String talla;
    private Integer cantidad;
    private Integer estado;
    private Byte productoStock;
    private int referenciaProducto;
    private int idVenta;

    @Id
    @Column(name = "idDetalleVenta")
    public int getIdDetalleVenta() {
        return idDetalleVenta;
    }

    public void setIdDetalleVenta(int idDetalleVenta) {
        this.idDetalleVenta = idDetalleVenta;
    }

    @Basic
    @Column(name = "talla")
    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    @Basic
    @Column(name = "cantidad")
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "estado")
    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @Basic
    @Column(name = "productoStock")
    public Byte getProductoStock() {
        return productoStock;
    }

    public void setProductoStock(Byte productoStock) {
        this.productoStock = productoStock;
    }

    @Basic
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Basic
    @Column(name = "idVenta")
    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbDetalleVentaEntity that = (TbDetalleVentaEntity) o;
        return idDetalleVenta == that.idDetalleVenta &&
                referenciaProducto == that.referenciaProducto &&
                idVenta == that.idVenta &&
                Objects.equals(talla, that.talla) &&
                Objects.equals(cantidad, that.cantidad) &&
                Objects.equals(estado, that.estado) &&
                Objects.equals(productoStock, that.productoStock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDetalleVenta, talla, cantidad, estado, productoStock, referenciaProducto, idVenta);
    }
}
