package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_tipos_accesorio", schema = "balartnuvies")
public class TbTiposAccesorioEntity {
    private int idTipoAccesorio;
    private String nombre;
    private String actualwrk;

    @Id
    @Column(name = "idTipoAccesorio")
    public int getIdTipoAccesorio() {
        return idTipoAccesorio;
    }

    public void setIdTipoAccesorio(int idTipoAccesorio) {
        this.idTipoAccesorio = idTipoAccesorio;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbTiposAccesorioEntity that = (TbTiposAccesorioEntity) o;
        return idTipoAccesorio == that.idTipoAccesorio &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTipoAccesorio, nombre, actualwrk);
    }
}
