package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_detalle_producto", schema = "balartnuvies")
public class TbDetalleProductoEntity {
    private int idDetalleProducto;
    private String talla;
    private Integer cantidad;
    private String propio;
    private String actualwrk;
    private int referenciaProducto;

    @Id
    @Column(name = "idDetalleProducto")
    public int getIdDetalleProducto() {
        return idDetalleProducto;
    }

    public void setIdDetalleProducto(int idDetalleProducto) {
        this.idDetalleProducto = idDetalleProducto;
    }

    @Basic
    @Column(name = "talla")
    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    @Basic
    @Column(name = "cantidad")
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "propio")
    public String getPropio() {
        return propio;
    }

    public void setPropio(String propio) {
        this.propio = propio;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbDetalleProductoEntity that = (TbDetalleProductoEntity) o;
        return idDetalleProducto == that.idDetalleProducto &&
                referenciaProducto == that.referenciaProducto &&
                Objects.equals(talla, that.talla) &&
                Objects.equals(cantidad, that.cantidad) &&
                Objects.equals(propio, that.propio) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDetalleProducto, talla, cantidad, propio, actualwrk, referenciaProducto);
    }
}
