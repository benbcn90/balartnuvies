package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_alertas_stock", schema = "balartnuvies")
public class TbAlertasStockEntity {
    private int idAlertaStock;
    private Byte avisar;
    private String actualwrk;
    private int referenciaProducto;

    @Id
    @Column(name = "idAlertaStock")
    public int getIdAlertaStock() {
        return idAlertaStock;
    }

    public void setIdAlertaStock(int idAlertaStock) {
        this.idAlertaStock = idAlertaStock;
    }

    @Basic
    @Column(name = "avisar")
    public Byte getAvisar() {
        return avisar;
    }

    public void setAvisar(Byte avisar) {
        this.avisar = avisar;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbAlertasStockEntity that = (TbAlertasStockEntity) o;
        return idAlertaStock == that.idAlertaStock &&
                referenciaProducto == that.referenciaProducto &&
                Objects.equals(avisar, that.avisar) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAlertaStock, avisar, actualwrk, referenciaProducto);
    }
}
