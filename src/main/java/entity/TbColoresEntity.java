package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_colores", schema = "balartnuvies")
public class TbColoresEntity {
    private int idColor;
    private String nombre;
    private String actualwrk;

    @Id
    @Column(name = "idColor")
    public int getIdColor() {
        return idColor;
    }

    public void setIdColor(int idColor) {
        this.idColor = idColor;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbColoresEntity that = (TbColoresEntity) o;
        return idColor == that.idColor &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idColor, nombre, actualwrk);
    }
}
