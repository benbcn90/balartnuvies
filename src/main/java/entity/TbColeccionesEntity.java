package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_colecciones", schema = "balartnuvies")
public class TbColeccionesEntity {
    private int idColeccion;
    private String nombre;
    private String anio;
    private String vigente;
    private String actualwrk;
    private int idMarca;

    @Id
    @Column(name = "idColeccion")
    public int getIdColeccion() {
        return idColeccion;
    }

    public void setIdColeccion(int idColeccion) {
        this.idColeccion = idColeccion;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "anio")
    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    @Basic
    @Column(name = "vigente")
    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "idMarca")
    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbColeccionesEntity that = (TbColeccionesEntity) o;
        return idColeccion == that.idColeccion &&
                idMarca == that.idMarca &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(anio, that.anio) &&
                Objects.equals(vigente, that.vigente) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idColeccion, nombre, anio, vigente, actualwrk, idMarca);
    }
}
