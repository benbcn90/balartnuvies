package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_tipos_escot", schema = "balartnuvies")
public class TbTiposEscotEntity {
    private int idTipoEscote;
    private String nombre;
    private String actualwrk;

    @Id
    @Column(name = "idTipoEscote")
    public int getIdTipoEscote() {
        return idTipoEscote;
    }

    public void setIdTipoEscote(int idTipoEscote) {
        this.idTipoEscote = idTipoEscote;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbTiposEscotEntity that = (TbTiposEscotEntity) o;
        return idTipoEscote == that.idTipoEscote &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTipoEscote, nombre, actualwrk);
    }
}
