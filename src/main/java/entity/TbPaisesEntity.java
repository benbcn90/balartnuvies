package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_paises", schema = "balartnuvies")
public class TbPaisesEntity {
    private int idPais;
    private String nombre;
    private String actualwrk;

    @Id
    @Column(name = "idPais")
    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbPaisesEntity that = (TbPaisesEntity) o;
        return idPais == that.idPais &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPais, nombre, actualwrk);
    }
}
