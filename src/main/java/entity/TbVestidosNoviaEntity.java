package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_vestidos_novia", schema = "balartnuvies")
public class TbVestidosNoviaEntity {
    private int idVestidosNovia;
    private Byte pedreria;
    private Byte cola;
    private String actualwrk;
    private int tbTiposVestidoIdTipoVestido;
    private int idTipoEscote;
    private int referenciaProducto;

    @Id
    @Column(name = "idVestidosNovia")
    public int getIdVestidosNovia() {
        return idVestidosNovia;
    }

    public void setIdVestidosNovia(int idVestidosNovia) {
        this.idVestidosNovia = idVestidosNovia;
    }

    @Basic
    @Column(name = "pedreria")
    public Byte getPedreria() {
        return pedreria;
    }

    public void setPedreria(Byte pedreria) {
        this.pedreria = pedreria;
    }

    @Basic
    @Column(name = "cola")
    public Byte getCola() {
        return cola;
    }

    public void setCola(Byte cola) {
        this.cola = cola;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Basic
    @Column(name = "tb_tipos_vestido_idTipoVestido")
    public int getTbTiposVestidoIdTipoVestido() {
        return tbTiposVestidoIdTipoVestido;
    }

    public void setTbTiposVestidoIdTipoVestido(int tbTiposVestidoIdTipoVestido) {
        this.tbTiposVestidoIdTipoVestido = tbTiposVestidoIdTipoVestido;
    }

    @Basic
    @Column(name = "idTipoEscote")
    public int getIdTipoEscote() {
        return idTipoEscote;
    }

    public void setIdTipoEscote(int idTipoEscote) {
        this.idTipoEscote = idTipoEscote;
    }

    @Basic
    @Column(name = "referenciaProducto")
    public int getReferenciaProducto() {
        return referenciaProducto;
    }

    public void setReferenciaProducto(int referenciaProducto) {
        this.referenciaProducto = referenciaProducto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbVestidosNoviaEntity that = (TbVestidosNoviaEntity) o;
        return tbTiposVestidoIdTipoVestido == that.tbTiposVestidoIdTipoVestido &&
                idTipoEscote == that.idTipoEscote &&
                referenciaProducto == that.referenciaProducto &&
                Objects.equals(pedreria, that.pedreria) &&
                Objects.equals(cola, that.cola) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pedreria, cola, actualwrk, tbTiposVestidoIdTipoVestido, idTipoEscote, referenciaProducto);
    }

}
