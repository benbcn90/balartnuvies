package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_marca", schema = "balartnuvies")
public class TbMarcaEntity {
    private int idMarca;
    private String nombre;
    private String vigente;
    private String actualwrk;

    @Id
    @Column(name = "idMarca")
    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "vigente")
    public String getVigente() {
        return vigente;
    }

    public void setVigente(String vigente) {
        this.vigente = vigente;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbMarcaEntity that = (TbMarcaEntity) o;
        return idMarca == that.idMarca &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(vigente, that.vigente) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMarca, nombre, vigente, actualwrk);
    }
}
