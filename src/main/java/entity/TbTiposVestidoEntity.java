package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tb_tipos_vestido", schema = "balartnuvies")
public class TbTiposVestidoEntity {
    private int idTipoVestido;
    private String nombre;
    private String actualwrk;

    @Id
    @Column(name = "idTipoVestido")
    public int getIdTipoVestido() {
        return idTipoVestido;
    }

    public void setIdTipoVestido(int idTipoVestido) {
        this.idTipoVestido = idTipoVestido;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "actualwrk")
    public String getActualwrk() {
        return actualwrk;
    }

    public void setActualwrk(String actualwrk) {
        this.actualwrk = actualwrk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TbTiposVestidoEntity that = (TbTiposVestidoEntity) o;
        return idTipoVestido == that.idTipoVestido &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(actualwrk, that.actualwrk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTipoVestido, nombre, actualwrk);
    }
}
